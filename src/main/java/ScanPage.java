import org.jsoup.HttpStatusException;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;

import java.io.IOException;
import java.net.SocketTimeoutException;
import java.util.ArrayList;
import java.util.List;

public class ScanPage {
    public static void scanPage(String link)throws IOException {
        String MAGAZIN = "https://tiflocentre.ru/magazin/";
        {
            try {
                Document document = Jsoup.connect(link).get();
                Elements elements = document.getElementsByAttributeValue("id", "wp-product-view");
                for (Element testelement : elements) {
                    try {
                        Element righttext = testelement.child(3);

                        String name = testelement.child(0).text();

                        String article = clearArticle(righttext.child(0).text());
                        String price = clearPrice(righttext.child(1).child(5).text());
                        Element art = testelement.getElementById("img-product-view");
                        String arthref = MAGAZIN + art.child(0).attr("href");
                        String description = getDiscription(testelement.getElementsByAttributeValue("class","m0-10 fSize14").first());

                        product product = new product(link,article,description,arthref,price,name);
                        System.out.print(".");
                        main.getProductList().add(product);

                    } catch (IndexOutOfBoundsException e) {
                        e.printStackTrace();
                    }
                }
            }catch (HttpStatusException e){
                e.printStackTrace();
            }
            catch (SocketTimeoutException e){
                e.printStackTrace();
            }
        }

    }

    public static String getDiscription(Element element){
        int count = element.childNodeSize();
        String output="";
        {
            try {
                for (int i = 0; i < count; i++) {
                    output+= " " + element.child(i).text();
                }
            }catch (IndexOutOfBoundsException e){
                return output;
            }

        }
        return output;

    }



    public static String clearArticle(String input){
        String out = input.substring(9);
        return out;
    }

    public static String clearPrice(String input){
        String out = input.replaceAll("[^0-9]", "");

        if(out.equals("")) out = "0";
        return out;
    }
}
