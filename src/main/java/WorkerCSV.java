import org.apache.commons.csv.CSVFormat;
import org.apache.commons.csv.CSVPrinter;

import javax.swing.*;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import static java.lang.System.out;

public class WorkerCSV extends JFrame {

    //Delimiter used in CSV file
    private static final String NEW_LINE_SEPARATOR = "\n";

    //CSV file header
    private static final Object [] FILE_HEADER = {  "Product code",  "Language", "Product id" , "Category", "List price", "Price"
            ,"Weight", "Quantity","Min quantity","Shipping freight", "Date added", "Downloadable", "Files"
            ,"Ship downloadable", "Inventory tracking", "Free shipping","Zero price action","Thumbnail"
            ,"Detailed image", "Product name", "Description", "Meta keywords","Meta description","Search words","Page title",
            "Taxes","Features", "Options", "Secondary categories","SEO name", "Short description","Status","Product URL", "Image URL"};


    public static void writeCSV(File file , List<product> productList){
        FileWriter fileWriter = null;

        CSVPrinter csvFilePrinter = null;

        try {

            //initialize FileWriter object
            fileWriter = new FileWriter(file);

            //initialize CSVPrinter object
            csvFilePrinter = new CSVPrinter(fileWriter, CSVFormat.DEFAULT );

            //Create CSV file header
            csvFilePrinter.printRecord(FILE_HEADER);

            //Write a new student object list to the CSV file
            for (product product : productList) {
                List productParameters = new ArrayList();
                productParameters.add(product.getArticle());
                productParameters.add("ru");
                productParameters.add("");
                productParameters.add("Помойка");
                productParameters.add("0.00");
                productParameters.add(product.getPrice());
                productParameters.add("0.000");
                productParameters.add("0");
                productParameters.add("0");
                productParameters.add("0.00");
                productParameters.add("");
                productParameters.add("N");
                productParameters.add("");
                productParameters.add("N");
                productParameters.add("B");
                productParameters.add("N");
                productParameters.add("R");
                productParameters.add("");
                productParameters.add("");
                productParameters.add(product.getName());
                productParameters.add(product.getDescription());
                productParameters.add("");
                productParameters.add("");
                productParameters.add("");
                productParameters.add("");
                productParameters.add("");
                productParameters.add("");
                productParameters.add("");
                productParameters.add("");
                productParameters.add("");
                productParameters.add("");
                productParameters.add("");
                productParameters.add("");
                productParameters.add(product.getArthref());
                csvFilePrinter.printRecord(productParameters);
            }
            controller.showInfo("Файл записан");

            System.out.println("CSV file was created successfully !!!");

        } catch (Exception e) {
            System.out.println("Error in CsvFileWriter !!!");
            controller.showInfo("Ошибка при записи файла #1");
            e.printStackTrace();
        } finally {
            try {
                fileWriter.flush();
                fileWriter.close();
                csvFilePrinter.close();
            } catch (IOException e) {
                System.out.println("Error while flushing/closing fileWriter/csvPrinter !!!");
                controller.showInfo("Ошибка при записи файла #2");
                e.printStackTrace();
            }
        }


    }


    public static File createFile(String name){
        String directory=pathFinder();
        File file = new File(directory +"/" + name + ".csv");
        {
            try {
                file.createNewFile();
            }catch (Exception e){
                e.printStackTrace();
            }
        }
        return file;

    }
    public static String pathFinder() {
        out.println("In pathfinder");
        JFileChooser fd = new JFileChooser();
        fd.setFileSelectionMode(JFileChooser.DIRECTORIES_ONLY);
        fd.showOpenDialog(new JFrame());
        File f = fd.getSelectedFile();
        String filename = f.getName();
        String path = f.getAbsolutePath().replaceAll(filename, "");
        return new String (path+filename);
    }
}
