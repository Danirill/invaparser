import java.util.ArrayList;
import java.util.List;

import javafx.application.Application;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.stage.Stage;
import javafx.scene.Scene;

public class main extends Application {
    static List<product> productList = new ArrayList<product>();
    public static List<product> getProductList() {
        return productList;
    }

    public static void main(String[] args) {
        Application.launch(args);
    }
    @Override
    public void start(Stage stage) throws Exception {
        FXMLLoader loader = new FXMLLoader();

        loader.setLocation(getClass().getResource("/fxml/mainActivity.fxml"));
        Parent root = loader.load();


        stage.setTitle("Сканнер");
        stage.setScene(new Scene(root, 500*1.5, 500));
        stage.show();
    }

}




class product{
    String href;
    String article;
    String description;
    String arthref;
    String price;
    String name;

    public String getName() {
        return name;
    }

    public product(String href, String article, String description, String arthref, String price, String name) {
        this.href = href;
        this.article = article;
        this.description = description;
        this.arthref = arthref;
        this.price = price;
        this.name = name;
    }

    public String getHref() {
        return href;
    }

    public void setHref(String href) {
        this.href = href;
    }

    public String getArticle() {
        return article;
    }

    public void setArticle(String article) {
        this.article = article;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getArthref() {
        return arthref;
    }

    public void setArthref(String arthref) {
        this.arthref = arthref;
    }

    public String getPrice() {
        return price;
    }

    public void setPrice(String price) {
        this.price = price;
    }
}
