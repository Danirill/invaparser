import javafx.application.Platform;
import javafx.concurrent.Task;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;

public class Analyser implements  Runnable {
    public void run(){
        controller.showInfo("Сканирование запущено");
        File file = WorkerCSV.createFile("name");
        ArrayList<String> links = ParserXML.getListOfPages();

        for(int i=0;i<links.size();i++){
            try {
                ScanPage.scanPage(links.get(i));
                controller.showInfo(String.valueOf(i+ "/"+links.size()));
            } catch (IOException e) {
                e.printStackTrace();
            }catch (IllegalStateException e){

            }
        }
        controller.showInfo("Все страницы отсканированы");

        WorkerCSV.writeCSV(file, main.productList);
    }
}
