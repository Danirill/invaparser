import org.w3c.dom.Document;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.xml.sax.SAXException;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import java.io.BufferedInputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.ArrayList;

public class ParserXML {
    public static ArrayList<String> getListOfPages() {
        ArrayList<String> outlist = new ArrayList<>();
        URL url;
        {
            try {
                url = new URL("https://tiflocentre.ru/sitemap.xml");

                downloadUsingStream(String.valueOf(url), "/home/danirill/IdeaProjects/excelhelper/sitemap.xml");
                File xmlfile = new File("/home/danirill/IdeaProjects/excelhelper/sitemap.xml");

                DocumentBuilder documentBuilder = DocumentBuilderFactory.newInstance().newDocumentBuilder();
                Document document = documentBuilder.parse(xmlfile);

                Node root = document.getDocumentElement();
                NodeList books = root.getChildNodes();

                for (int i = 0; i < books.getLength(); i++) {
                    Node book = books.item(i);
                    if (book.getNodeType() != Node.TEXT_NODE) {
                        NodeList bookProps = book.getChildNodes();
                        for (int j = 0; j < bookProps.getLength(); j++) {
                            Node bookProp = bookProps.item(j);
                            if (bookProp.getNodeType() != Node.TEXT_NODE) {
                                if(idFilter(bookProp.getChildNodes().item(0).getTextContent())){
                                    outlist.add(bookProp.getChildNodes().item(0).getTextContent());
                                }
                            }
                        }
                    }
                }
            } catch (MalformedURLException e) {
                e.printStackTrace();
            } catch (IOException e) {
                e.printStackTrace();
            } catch (ParserConfigurationException e) {
                e.printStackTrace();
            } catch (SAXException e) {
                e.printStackTrace();
            }
        }
        controller.showInfo("Карта сайта изучена");
        return outlist;
    }

    public static boolean idFilter(String input){
        if(input.indexOf("&id=")!=-1){
            return true;
        }else  return false;
    }

    private static void downloadUsingStream(String urlStr, String file) throws IOException{
        URL url = new URL(urlStr);
        BufferedInputStream bis = new BufferedInputStream(url.openStream());
        FileOutputStream fis = new FileOutputStream(file);
        byte[] buffer = new byte[1024];
        int count=0;
        while((count = bis.read(buffer,0,1024)) != -1)
        {
            fis.write(buffer, 0, count);
        }
        fis.close();
        bis.close();
        controller.showInfo("Карта сайта получена");
    }
}
