import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.ListView;
import javafx.scene.control.ProgressBar;

import java.awt.*;
import java.io.IOException;
import java.net.URL;
import java.util.ResourceBundle;

public  class controller {
    @FXML
    public static ProgressBar progressBar;
    @FXML
    private ListView<String> listView;

    private static ObservableList observableList = FXCollections.observableArrayList();

    @FXML
    private void startScanning(ActionEvent event) throws IOException {

        listView.setItems(observableList);
        try{Runnable runnable = new Analyser();
            new Thread(runnable).start();
        }catch (IllegalStateException e){
            e.printStackTrace();
        }


    }

    public static void showInfo(String string){
        observableList.add(string);
    }

}
