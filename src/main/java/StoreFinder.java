import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

public  class StoreFinder {
    public static ArrayList<String> getListOfPages()throws IOException {
        String MAGAZIN = "https://tiflocentre.ru/magazin/view_katalog.php";
        ArrayList<String> strings = new ArrayList<>();
        Document document = Jsoup.connect(MAGAZIN).get();
        Elements linkContainers = document.getElementsByAttributeValue("class", "wp_link_cat");
        for (Element div : linkContainers) {
            Elements innerLinks = div.getAllElements();
            for(Element inblock : innerLinks){
                String link= inblock.attr("href");
                if(podCatFilter(link)){
                    strings.add(link);
                    System.out.println(link);
                }

            }
        }
        return strings;
    }

    public static boolean podCatFilter(String input){
        if(input.indexOf("&podcat=")!=-1){
            return true;
        }else  return false;
    }

}

